# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 20:39:27 2020

@author: Felipe Cruz
"""

import numpy as np

# Initialize parameters
gamma = 0.75 # Discount factor 
alpha = 0.2 # Learning rate 

# Define the states
location_to_state = {"BANHO 1" : 0, "COZINHA" : 1, "SALA DE ESTAR" : 2, 'VARANDA' : 3, 'SUITE' : 4, 'S. DE JANTAR' : 5, 'BANHO 2' : 6, 'QUARTO 1' : 7, 'QUARTO 2' : 8 }

# Define the rewards
rewards = np.array([[0,0,0,0,1,0,0,0,0],
              [0,0,0,0,0,1,0,0,0],
              [0,0,0,1,0,1,0,0,0],
              [0,0,1,0,0,0,0,0,0],
              [1,0,0,0,0,1,0,0,0],
              [0,1,1,0,1,0,1,1,1],
              [0,0,0,0,0,1,0,0,0],
              [0,0,0,0,0,1,0,0,0],
              [0,0,0,0,0,1,0,0,0]])

# Maps indices to locations
state_to_location = dict((state,location) for location,state in location_to_state.items())

# Define the actions
actions = [0,1,2,3,4,5,6,7,8]

def get_optimal_route(start_location,end_location):
    # Copy the rewards matrix to new Matrix
    rewards_new = np.copy(rewards)
    
    # Get the ending state corresponding to the ending location as given
    ending_state = location_to_state[end_location]    
    
    # With the above information automatically set the priority of the given ending state to the highest one
    rewards_new[ending_state,ending_state] = 999   
    
    # -----------Q-Learning algorithm-----------   
    # Initializing Q-Values
    Q = np.array(np.zeros([9,9]))

    # Q-Learning process
    for i in range(1000):
        # Pick up a state randomly
        current_state = np.random.randint(0,9) # Python excludes the upper bound

        # For traversing through the neighbor locations in the maze
        playable_actions = []
        # Iterate through the new rewards matrix and get the actions > 0
        for j in range(9):
            if rewards_new[current_state,j] > 0:
                playable_actions.append(j)

        # Pick an action randomly from the list of playable actions leading us to the next state
        
        next_state = np.random.choice(playable_actions)
        # Compute the temporal difference
        # The action here exactly refers to going to the next state
        TD = rewards_new[current_state,next_state] + gamma * Q[next_state, np.argmax(Q[next_state,])] - Q[current_state,next_state]
        
        # Update the Q-Value using the Bellman equation           
        Q[current_state,next_state] += alpha * TD
                 
    # Initialize the optimal route with the starting location
    route = [start_location]
    
    # We do not know about the next location yet, so initialize with the value of starting location
    next_location = start_location
    
    # Creating the ideal path.
    
    while(next_location != end_location):
        
        # Fetch the starting state
        starting_state = location_to_state[start_location]
        #print(starting_state)
        # Fetch the highest Q-value pertaining to starting state
        
        next_state = np.argmax(Q[starting_state,])        
        # We got the index of the next state. But we need the corresponding letter. 
        next_location = state_to_location[next_state]        
        route.append(next_location)
        
        # Update the starting location for the next iteration
        start_location = next_location

    print(np.around(Q, decimals=2))

    return route

print('FinalResult:', get_optimal_route('BANHO 1', 'VARANDA'))
