# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 09:35:05 2020

@author: FOC
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 20:37:22 2020
 
@author: FOC
"""
 
import random
import numpy as np
 
#########################################
# Criação da solução de referência.
def create_reference_solution():
    
    numero_decimal = 18062020   
    
    # Construindo um array com o valor de 18062020 em binário
    # [1,0,0,0,1,0,0,1,1,1,0,0,1,1,0,1,0,1,1,0,0,0,1,0,0]
    reference = np.array([int(x) for x in bin(numero_decimal)[2:]]) 
    
    return reference
#########################################
    
# Criando uma população
def create_starting_population(individuals, chromosome_length):
    # COnfigurando o array inicial de zeros.
    population = np.zeros((individuals, chromosome_length))
    # Passando por cada linha.
    for i in range(individuals):
        # Escolhendo um número aleatório de uns.
        ones = random.randint(0, chromosome_length)
        # ALternando zeros para uns.
        population[i, 0:ones] = 1
        # Misturando a linha.
        np.random.shuffle(population[i])
    
    return population
#########################################
 
# Cálculo de Fitness
# Alelos iguais contam pontos!
def calculate_fitness(reference, population):
    # Criando um array de True/False, ao comparar com a referência.
    identical_to_reference = population == reference
    # Soma dos alelos idênticos à referência.
    fitness_scores = identical_to_reference.sum(axis=1)
    
    return fitness_scores
#########################################
    
 
# Fazendo a seleção com Tournament Selection
def select_individual_by_tournament(population, scores):
    # Capturando o tamanho da população.
    population_size = len(scores)
    
    # Escolhendo candidados para o torneio.
    fighter_1 = random.randint(0, population_size-1)
    fighter_2 = random.randint(0, population_size-1)
    
    # Calcula o Fitness de cada candidato.
    fighter_1_fitness = scores[fighter_1]
    fighter_2_fitness = scores[fighter_2]
    
    # Identificando o candidato com maior Fitness
    # O candidato 1 vencerá em caso de empate.
    if fighter_1_fitness >= fighter_2_fitness:
        winner = fighter_1
    else:
        winner = fighter_2
    
    # Returna o vencedor.
    return population[winner, :]
#########################################
    
# Crossover
def breed_by_crossover(parent_1, parent_2):
    # Capturando o tamanho da população.
    chromosome_length = len(parent_1)
    
    # Definindo o ponto de crossover aleatoriamente, evitando os extremos.
    crossover_point = random.randint(1,chromosome_length-1)
    
    # Criando os filhos.
    # 'np.hstack' concatena dois arrays.
    child_1 = np.hstack((parent_1[0:crossover_point],
                        parent_2[crossover_point:]))
    
    child_2 = np.hstack((parent_2[0:crossover_point],
                        parent_1[crossover_point:]))
    
    # Retornando dois filhos.
    return child_1, child_2
#########################################
    
# Mutação
def randomly_mutate_population(population, mutation_probability):
    
    # Aplicando uma mutação aleatória.
    random_mutation_array = np.random.random(size=(population.shape))
    #print(random_mutation_array)
    random_mutation_boolean = random_mutation_array <= mutation_probability
    #print(random_mutation_boolean)
    population[random_mutation_boolean] = np.logical_not(population[random_mutation_boolean])
    #print(randomly_mutate_population)
    
    # Retorna a população após a mutação.
    return population
#########################################
                
# Configurando os parâmetros principais.
population_size = 500
maximum_generation = 30
best_score_progress = [] # Acompanhamento de progresso.
 
# Criando a solução de referência. 
# (para ilustrar o processo)
reference = create_reference_solution()
 
# Chromosome_length é a qtd de digitos do num binário
chromosome_length = len(reference)
 
# Criando a população inicial.
population = create_starting_population(population_size, chromosome_length)
 
# Apresentando o melhor score na população inicial.
scores = calculate_fitness(reference, population)
best_score = np.max(scores)/chromosome_length * 100
print ('Melhor percentual de acerto INICIAL: %.1f' %best_score)
 
# Adicionando este melhor score a um registro de progresso.
best_score_progress.append(best_score)
 
# Executando as gerações do GA.
for generation in range(maximum_generation):
    # Criando uma lista inicicl vazia para a população inicial.
    new_population = []
    
    # Criando uma nova população de dois em dois filhos.
    for i in range(int(population_size/2)):
        parent_1 = select_individual_by_tournament(population, scores)
        parent_2 = select_individual_by_tournament(population, scores)
        child_1, child_2 = breed_by_crossover(parent_1, parent_2)
        new_population.append(child_1)
        new_population.append(child_2)
    
    # Trocando a população antiga pela nova.
    population = np.array(new_population)
    
    # Aplicando a mutação.
    mutation_rate = 0.00000000000000002
    population = randomly_mutate_population(population, mutation_rate)
 
    # Gerando o score da melhor solução e adicionando ao registro.
    scores = calculate_fitness(reference, population)
    best_score = np.max(scores)/chromosome_length * 100
    best_score_progress.append(best_score)
 
# O algoritmo completou a geração.
print ('Melhor percentual de acerto FINAL: %.1f' %best_score)
 
# Visualizando o progresso.
import matplotlib.pyplot as plt
plt.plot(best_score_progress)
plt.xlabel('Geracao')
plt.ylabel('Melhor score (% target)')
plt.show()
